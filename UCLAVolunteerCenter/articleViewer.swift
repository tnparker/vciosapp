//
//  articleViewer.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor Parker on 8/24/16.
//  Copyright © 2016 UCLA. All rights reserved.
//


import UIKit

class articleViewer:UIViewController {
    
    @IBOutlet weak var webV: UIWebView!
    
    var urlString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let url = URL (string: urlString);
        let requestObj = URLRequest(url: url!);
        webV.loadRequest(requestObj);
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        print("ASDFASDFASD")
        self.dismiss(animated: true, completion: nil)
    }
}
