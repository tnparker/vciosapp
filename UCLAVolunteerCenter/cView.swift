//
//  cView.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor on 11/16/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import Foundation
import JTAppleCalendar

class cView:JTAppleDayCellView{
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var todayView: UIView!
    @IBOutlet var eventView: UIView!
    @IBOutlet var eventViewTwo: UIView!
    @IBOutlet var eventPlus: UILabel!
}
