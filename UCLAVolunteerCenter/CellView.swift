//
//  CellView.swift
//  UCLAVolunteerCenter
//
//  Created by Kathleen Ly on 8/30/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import Foundation
import UIKit

import JTAppleCalendar

class CellView: JTAppleDayCellView {
    @IBOutlet var dayLabel: UILabel!
    
    var normalDayColor = UIColor.black
    var weekendDayColor = UIColor.gray
    
    
    func setupCellBeforeDisplay(_ cellState: CellState, date: Date) {
        
        // Setup Cell text
        dayLabel.text =  cellState.text
        
        // Setup text color
        configureTextColor(cellState)
    }
    
    func configureTextColor(_ cellState: CellState) {
        if cellState.dateBelongsTo == .ThisMonth {
            dayLabel.textColor = normalDayColor
        } else {
            dayLabel.textColor = weekendDayColor
        }
    }
}
