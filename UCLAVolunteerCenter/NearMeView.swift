//
//  SecondViewController.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor Parker on 8/17/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import UIKit
import GoogleMaps
import CZPicker

class NearMeView: UIViewController, GMSMapViewDelegate,  CZPickerViewDelegate, CZPickerViewDataSource{
    
    var mapView : GMSMapView!
    var markers:Array< dataMarkers > = Array < dataMarkers >()
    var markerObjects:Array< GMSMarker > = Array < GMSMarker >()
    var theCond = true
    var arbVal = 1000000
    var selectedValUrl = ""
    
    var distancePicker = [String]()
    var catPicker = [String]()
    var pickerList = [String]()
    var which = ""
    var pickerWithImage: CZPickerView?
    
    var theLocation:CLLocation?
    var circle: GMSCircle!
    var catSet = "none"
    var distSet = 0
    var radius = Double(0)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 34.0689° N, 118.4452° W
        let camera = GMSCameraPosition.camera(withLatitude: 34.0689, longitude: -118.4452, zoom: 13.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        if let mylocation = mapView.myLocation {
            theLocation = mylocation
            print("User's location: \(mylocation)")
        } else {
            theLocation = CLLocation(latitude: 34.0689, longitude: -118.4452)
            print("User's location is unknown")
        }
        
        distancePicker = ["None", "5mi", "10mi", "15mi", "20mi", "25mi"]
        catPicker = ["None", "Art", "At Risk Youth", "Disabilities", "Disaster Relief", "Education", "Environment", "Health", "Hunger", "International", "Mentoring", "Poverty", "Seniors", "Sustainability", "Tutoring"]
        
        view = mapView
        
        self.mapView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //fake params are fun params
        let parameters = ["Username":"Admin", "Password":"123","DeviceId":"87878"] as Dictionary<String, String>
        let url = "https://volunteer.ucla.edu/external/api?events&0w4yPxDG0Ub2E9yy589FZvovSP42OqGL&iphone"
        post(parameters, url: url)
        
        while theCond {
            if(markers.count == arbVal){
                for data in self.markers{
                    
                    let position = CLLocationCoordinate2D(latitude: data.latitude, longitude: data.longitude)
                    let marker = GMSMarker(position: position)
                    marker.title = data.title
                    marker.snippet = data.desc
                    marker.userData = data.url
                    marker.map = mapView
                    
                    markerObjects.append(marker)
                }
                
                theCond = false
            }
            
            print(markers.count)
            
        }
        
        
        let distButton = UIButton(frame: CGRect(x: 10, y: 20, width: 100, height: 50))
        distButton.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 0.9)
        distButton.setTitleColor(UIColor(red: 14/255, green: 122/255, blue: 254/255, alpha: 1), for: UIControlState())
        distButton.setTitle("Distance", for: UIControlState())
        distButton.addTarget(self, action: #selector(distanceTouch), for: .touchUpInside)
        
        self.view.addSubview(distButton)
        
        let catButton = UIButton(frame: CGRect(x: 130, y: 20, width: 100, height: 50))
        catButton.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 0.9)
        catButton.setTitleColor(UIColor(red: 14/255, green: 122/255, blue: 254/255, alpha: 1), for: UIControlState())
        catButton.setTitle("Category", for: UIControlState())
        catButton.addTarget(self, action: #selector(catTouch), for: .touchUpInside)
        
        self.view.addSubview(catButton)
        
    }
    
    func distanceTouch() {
        pickerList = distancePicker
        which = "distance"
        let picker = CZPickerView(headerTitle: "Distance", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
        picker?.delegate = self
        picker?.dataSource = self
        picker?.needFooterView = false
        picker?.headerBackgroundColor = UIColor(red: 0/255, green: 165/255, blue: 229/255, alpha: 1)
        picker?.show()
    }
    
    func catTouch() {
        pickerList = catPicker
        which = "category"
        let picker = CZPickerView(headerTitle: "Filter by Category", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
        picker?.delegate = self
        picker?.dataSource = self
        picker?.needFooterView = false
        picker?.headerBackgroundColor = UIColor(red: 0/255, green: 165/255, blue: 229/255, alpha: 1)
        picker?.show()
    }
    
    func numberOfRows(in pickerView: CZPickerView!) -> Int {
        return pickerList.count
    }
    
    func czpickerView(_ pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        return pickerList[row]
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemAtRow row: Int){
        //print(pickerList[row])
        
        
        var i = 0
        if(which == "distance"){
            
            radius = Double(row) * 1609
            
            let circleCenter = CLLocationCoordinate2D(latitude: (theLocation?.coordinate.latitude)!, longitude: (theLocation?.coordinate.longitude)!)
            
            
            if(circle != nil){
                circle.map = nil
            }
            
            
            if(row > 0){
                circle = GMSCircle(position: circleCenter, radius: radius )
                circle.fillColor = UIColor(red: 0/255, green: 165/255, blue: 229/255, alpha: 0.6)
                circle.strokeColor = UIColor(red: 50/255, green: 132/255, blue: 191/255, alpha: 0.8)
                circle.map = mapView
                
            }
            
            if(radius != 0){
                let southwest = GMSGeometryOffset(circleCenter, radius * sqrt(2.0), 225);
                let northeast = GMSGeometryOffset(circleCenter, radius * sqrt(2.0), 45);
                // pan to see all markers on map:
                let bounds = GMSCoordinateBounds.init(coordinate: southwest, coordinate: northeast)

                mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 10))
            }
        }
        
        for marker in markers {
            let markerLocation = CLLocation(latitude: marker.latitude, longitude: marker.longitude)
            let distance = markerLocation.distance(from: theLocation!)
            
            if(which == "distance") {
                
                if(marker.tags.lowercased().range(of: catSet) != nil || catSet == "none") {
                    if (radius != 0) {
                        if (distance > radius) {
                            markerObjects[i].map = nil
                        } else {
                            markerObjects[i].map = mapView
                        }
                    } else {
                        markerObjects[i].map = mapView
                    }
                }
                else{
                    markerObjects[i].map = nil
                }
            }
            else {
                catSet = pickerList[row].lowercased()
                if(distance < radius || radius == 0) {
                    if (catSet != "none") {
                        if (marker.tags.lowercased().range(of: catSet) != nil) {
                            markerObjects[i].map = mapView
                        } else {
                            markerObjects[i].map = nil
                        }
                    } else {
                        markerObjects[i].map = mapView
                    }
                }
                else{
                    markerObjects[i].map = nil
                }
            }
            
            i += 1;
            //marker.remove(); <-- works too!
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func czpickerViewDidClickCancelButton(_ pickerView: CZPickerView!) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemsAtRows rows: [AnyObject]!) {
        for row in rows {
            if let row = row as? Int {
                print(pickerList[row])
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        selectedValUrl = (mapView.selectedMarker?.userData)! as! String
        print(selectedValUrl)
        performSegue(withIdentifier: "oppviewer", sender: "notimportant")
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let subCategory:oppViewer = segue.destination as! oppViewer
        
        subCategory.urlString = selectedValUrl
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func post(_ params : Dictionary<String, String>, url : String) {
        let request = NSMutableURLRequest(url: URL(string: url)!)
        
        let session = URLSession.shared
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
        } catch {
            print(error)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard data != nil else {
                print("no data found: \(error)")
                return
            }
            
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                    if let events = json["event"] as? [[String: AnyObject]] {
                        self.arbVal = events.count
                        for evt in events {
                            let name = evt["title"] as? String
                            let full = evt["descriptionFull"] as? String
                            //let howDays = evt["howDays"] as? String
                            let startend = evt["startEnd"] as? String
                            let url = evt["oppUrl"] as? String
                            //let fullAddress = evt["fullAddress"] as? String
                            /*let fullName = evt["name"] as? String
                            let phone = evt["phone"] as? String
                            let email = evt["email"] as? String
                            //let org = evt["org"] as? String*/
                            let tags = evt["tags"] as? String
                            let long = evt["longitude"]?.doubleValue
                            let lat = evt["latitude"]?.doubleValue
                            
                            let fullSnippet = startend! + "\n" + full!
                            /*fullSnippet +=  fullName! + " | " + phone! + " | " + email!
                            fullSnippet += "\n" + full!*/
                            
                            let newData = dataMarkers.init(t: name!, d: fullSnippet, tgs: tags!, u: url!, lat: lat!, long: long!)
                            
                            self.markers.append(newData)
                        }
                        
                    }
                    
                    
                } else {
                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
                    print("NSError could not parse JSON: \(jsonStr)")
                }
            } catch let parseError {
                print(parseError)                                                          // Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            
        }) 
        
        task.resume()
      
    }


}

