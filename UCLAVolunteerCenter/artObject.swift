//
//  artObject.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor Parker on 8/24/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import Foundation

class artObject {
    
    var title:String
    var desc:String
    var url:String
    
    init(t:String, d:String, u:String){
        self.title = t
        self.desc = d
        self.url = u
    }
    
}
