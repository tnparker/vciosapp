//
//  calHeader.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor on 11/28/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import Foundation

import JTAppleCalendar
class calHeader: JTAppleHeaderView {
    @IBOutlet var title: UILabel!
    @IBOutlet var yearTitle: UILabel!
}
