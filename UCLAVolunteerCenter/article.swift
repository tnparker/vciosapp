//
//  article.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor Parker on 8/23/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import UIKit

class article: UITableViewCell {
// MARK: Properties
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
