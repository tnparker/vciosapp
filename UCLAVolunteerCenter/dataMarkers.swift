//
//  dataMarkers.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor Parker on 8/25/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import Foundation

class dataMarkers {
    var title:String
    var desc:String
    var url:String
    var tags:String
    var latitude:Double
    var longitude:Double
    
    init(t:String, d:String, tgs:String, u:String, lat:Double, long:Double){
        self.title = t
        self.desc = d
        self.tags = tgs
        self.url = u
        self.latitude = lat
        self.longitude = long
    }
}
