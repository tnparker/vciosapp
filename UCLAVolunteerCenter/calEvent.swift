//
//  calEvent.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor on 11/28/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import Foundation

class calEvent {
    var title:String
    var desc:String
    var url:String
    var tags:String
    var start:Date
    var startend:String
    var contact:String
    var location:String
    
    init(t:String, d:String, tgs:String, u:String, st:Date, se:String, cont:String, adr:String){
        self.title = t
        self.desc = d
        self.tags = tgs
        self.url = u
        self.start = st
        self.startend = se
        self.contact = cont
        self.location = adr
    }
}
