//
//  FirstViewController.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor Parker on 8/17/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import UIKit
import SDWebImage

class HomeView: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var isGood = true;
    var articles:Array< artObject > = Array < artObject >()
    var articleUrls:Array< String > = Array < String >()
    let dDown = ["About", "Credits", "Help"]
    let bonusInfo = ["This app was created to help UCLA Volunteers find opportunities and stay up to date with Volunteer Center content. Our mission is to inspire the more than 400,000 members of the extended UCLA family, as well as the community at large, to create social change through lifelong participation in volunteer programs and civic engagement.", "Thank you to the:\n-SD Web Image\n-Google Maps\n-JTAppleCalendar\n-CZPicker\ndevelopers for their content that helped create app features. \n\nThis app is the property of UCLA Volunteer Center.\nCreated by Taylor Parker.", "Newsfeed: Shows the UCLA Volunteer Center's recent articles.\n\nVolunteer: A location based volunteer opportunity finder that pulls opportunities directly from the UCLA Volunteer Center Database.\n\nPrograms: All Volunteer Center programs, right at your fingertips!\n\nVolunteer Day: Our pride and joy, Volunteer Day. Contains information about Volunteer Day from 2009 to now.\n\nCalendar: All single day volunteer opportunities in calendar format. Also has the ability to add events to your personal calendar.\n\nHelp us!\nDid the app break?\nDo you have a feature suggestion?\nEmail Taylor Parker at tparker@volunteer.ucla.edu"]
    
    var notDisplayed = true
    
    weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var dropDownTView: UITableView!
    @IBOutlet var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        tableview.backgroundView = activityIndicatorView
        self.activityIndicatorView = activityIndicatorView
        activityIndicatorView.startAnimating()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.dropDownTView.contentInset = UIEdgeInsetsMake(-37, 0, -25, 0);
        
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    override func viewDidAppear(_ animated: Bool) {
        
            let parameters = ["Username":"Admin", "Password":"123","DeviceId":"87878"] as Dictionary<String, String>
            let url = "https://volunteer.ucla.edu/external/api?news&0w4yPxDG0Ub2E9yy589FZvovSP42OqGL&iphone"
            post(parameters, url: url)
        
        //dropDownTView.frame = CGRect(x: dropDownTView.frame.origin.x, y: dropDownTView.frame.origin.y, width: dropDownTView.frame.size.width, height: dropDownTView.contentSize.height)
        self.tableview.rowHeight = UITableViewAutomaticDimension;
        self.tableview.estimatedRowHeight = 140.0; // set to whatever your "average" cell height is
    }
    
    
    func post(_ params : Dictionary<String, String>, url : String) {
        let request = NSMutableURLRequest(url: URL(string: url)!)
        
        let session = URLSession.shared
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
         } catch {
            print(error)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard data != nil else {
                print("no data found: \(error)")
                return
            }
            
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                    if let articles = json["articles"] as? [[String: AnyObject]] {
                        for article in articles {
                            let name = article["title"] as? String
                            let source = article["source"] as? String
                            let postedOn = article["postedOn"] as? String
                            let snippet = article["descriptionReadMore"] as? String
                            let full = article["descriptionFull"] as? String
                            let url = article["picUrl"] as? String
                            let artUrl = article["artUrl"] as? String
                            
                            var finalString = source! + " | Posted on: " + postedOn!
                            finalString += "\n" + snippet! + "\n -> Read More"
                            
                            var finalDesc = source! + " | Posted on: " + postedOn!
                            finalDesc += "\n" + full!
                            
                            let finalUrl = url?.replacingOccurrences(of: "-160x160", with: "")
                            
                            let obj = artObject.init(t: name!, d: finalString, u: finalUrl!)
                            
                            self.articles.append(obj)
                            self.articleUrls.append(artUrl!)
                            
                        }
                    }
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.tableview.reloadData()
                    })
                } else {
                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
                    print("NSError could not parse JSON: \(jsonStr)")
                }
            } catch let parseError {
                print(parseError)                                                          // Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
        }) 
        
        task.resume()
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of items in the sample data structure.
        
        var count = 0
        
        if tableView == self.tableview {
            count = self.articles.count
        }
        
        if tableView == self.dropDownTView{
            count = self.dDown.count
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == self.tableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "customcell", for: indexPath) as! article
            
            let imageView = cell.imgView
            let nameL = cell.nameLabel
            let descL = cell.descLabel
            // example, this will probably come from elsewhere in your app
            
            imageView?.sd_setImage(with: URL(string: self.articles[indexPath.row].url), placeholderImage: UIImage(named: "imgPlaceholder"))
            
            nameL?.text = articles[indexPath.row].title
            descL?.text = articles[indexPath.row].desc
            
            return cell
        }
            // if tableView == self.dropDownTView
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "dropCell", for: indexPath)
            
            cell.textLabel?.text = dDown[indexPath.row]
            
            return cell
        }

        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.dropDownTView {
            DispatchQueue.main.async(execute: { () -> Void in
                let alert = UIAlertController(title: self.dDown[indexPath.row], message: self.bonusInfo[indexPath.row], preferredStyle: UIAlertControllerStyle.actionSheet)
                
                if(indexPath.row == 2){
                    //Create and add "email" action
                    let emailAction: UIAlertAction = UIAlertAction(title: "Email Developer", style: .default) { action -> Void in
                        //email Taylor
                        let email = "tparker@volunteer.ucla.edu"
                        let url = NSURL(string: "mailto:\(email)")
                        UIApplication.shared.openURL(url as! URL)
                    }
                
                    alert.addAction(emailAction)
                }
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })

        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
            let subCategory:articleViewer = segue.destination as! articleViewer
            
            if let indexPath = tableview.indexPathForSelectedRow {
                // do the work here
                
                let artObj = articleUrls[indexPath.row]
                
                subCategory.urlString = artObj
            }
    }
    
    @IBAction func openClose(_ sender: AnyObject) {
        if(notDisplayed){
            dropDownTView.isHidden = false
            notDisplayed = false
        }
        else{
            dropDownTView.isHidden = true
            notDisplayed = true
        }
    }

}

