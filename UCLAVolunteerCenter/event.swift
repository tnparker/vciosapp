//
//  event.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor on 12/13/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import UIKit

class event: UITableViewCell {
    // MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var locContLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
