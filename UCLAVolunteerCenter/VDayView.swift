//
//  VDayView.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor Parker on 8/19/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import UIKit

class VDayView: UIViewController {

    @IBOutlet weak var webV: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Do any additional setup after loading the view.
        let url = URL (string: "https://volunteer.ucla.edu/volunteer-day/");
        let requestObj = URLRequest(url: url!);
        webV.loadRequest(requestObj);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
