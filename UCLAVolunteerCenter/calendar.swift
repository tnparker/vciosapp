//
//  calendar.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor on 11/16/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import Foundation
import JTAppleCalendar
import EventKit

class calendar: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var eventList: UITableView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    //weak var activityIndicatorView: UIActivityIndicatorView!
    
    var runOnce = true
    
    var events:Array< calEvent > = Array < calEvent >()
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November","December"]
    
    var colors = [UIColor(red:0.00, green:0.82, blue:0.20, alpha:1.0), UIColor(red:0.00, green:0.52, blue:0.94, alpha:1.0), UIColor(red:0.00, green:0.82, blue:0.20, alpha:1.0), UIColor(red:0.00, green:0.82, blue:0.20, alpha:1.0), UIColor(red:0.00, green:0.82, blue:0.20, alpha:1.0)]
    
    var numMonth = 10000
    var fromData = false
    var dateEvtCount = 0
    var dateIndex = 0
    var startDates:Array< Date > = Array < Date >()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendarView.dataSource = self
        calendarView.delegate = self
        calendarView.registerCellViewXib(file: "cell") // Registering your cell is manditory
        calendarView.registerHeaderView(xibFileNames: ["calHeader"])
        calendarView.cellInset = CGPoint(x: 1, y: 1)
        eventList.rowHeight = UITableViewAutomaticDimension
        eventList.estimatedRowHeight = 105.0;
        
        /*let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        self.backgroundView = activityIndicatorView
        self.activityIndicatorView = activityIndicatorView
        activityIndicatorView.startAnimating()*/
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(runOnce){
            //fake params are fun params
            let parameters = ["Username":"Admin", "Password":"123","DeviceId":"87878"] as Dictionary<String, String>
            let url = "https://volunteer.ucla.edu/external/api?events&0w4yPxDG0Ub2E9yy589FZvovSP42OqGL&iphone"
            post(parameters, url: url)
            runOnce = false
        }
    }
    
    func post(_ params : Dictionary<String, String>, url : String) {
        let request = NSMutableURLRequest(url: URL(string: url)!)
        
        let session = URLSession.shared
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
        } catch {
            print(error)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard data != nil else {
                print("no data found: \(error)")
                return
            }
            
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                    if let events = json["event"] as? [[String: AnyObject]] {
                        for evt in events {
                            let name = evt["title"] as? String
                            let full = evt["descriptionFull"] as? String
                            //let howDays = evt["howDays"] as? String
                            let url = evt["oppUrl"] as? String
                            let startend = evt["startEnd"] as? String
                            let fullNameArr = startend?.components(separatedBy: " - ")
                            let start = fullNameArr?[0]
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "MMM d, y"
                            let startdate = dateFormatter.date(from: start!)
                            //let end = fullNameArr?[1] as? Date
                            let fullAddress = evt["fullAddress"] as? String
                            let fullName = evt["name"] as? String
                             let phone = evt["phone"] as? String
                             let email = evt["email"] as? String
                            
                            let contactInform = fullName! + " | " + phone! + " | " + email!
                             //let org = evt["org"] as? String
                             
                             /*let long = evt["longitude"]?.doubleValue
                             let lat = evt["latitude"]?.doubleValue
                             */
                            let tags = evt["tags"] as? String
                            
                            //let fullSnippet = startend! + "\n" + full!
                            /*fullSnippet +=  fullName! + " | " + phone! + " | " + email!
                             fullSnippet += "\n" + full!*/
                            
                            let newData = calEvent.init(t: name!, d: full!, tgs: tags!, u: url!, st: startdate!, se: startend!, cont:contactInform, adr: fullAddress!)
                            
                            self.events.append(newData)
                            self.startDates.append(startdate!)
                            
                        }
                        self.events.sort(by: { $0.start.compare($1.start) == .orderedAscending})
                        self.startDates.sort(by: { $0.compare($1) == .orderedAscending})
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.calendarView.reloadData()
                            self.activityIndicator.stopAnimating()
                        })
                        print("Ready")
                    }
                    
                    
                } else {
                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
                    print("NSError could not parse JSON: \(jsonStr)")
                }
            } catch let parseError {
                print(parseError)                                                          // Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            
        })
        
        task.resume()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dateEvtCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! event
        let currentInfo = self.events[self.dateIndex + indexPath.item]
        
        cell.nameLabel?.text = currentInfo.title + " | " + currentInfo.startend
        
        //cell.descLabel?.numberOfLines = 0
        cell.descLabel?.text = currentInfo.desc
        
        cell.locContLabel?.text = currentInfo.contact
        
        cell.colorLabel.backgroundColor = colors[indexPath.item]
        
        cell.addButton.tag = self.dateIndex + indexPath.item
        cell.addButton.addTarget(self, action: #selector(newEvent), for: .touchUpInside)

        return cell
    }
    
    func newEvent(sender:UIButton){
        
        let currentInfo = self.events[sender.tag]
        let eventStore : EKEventStore = EKEventStore()
    
            eventStore.requestAccess(to: EKEntityType.event, completion: {
                (granted, error) in
    
                    if (granted) && (error == nil) {
                        print("granted \(granted)")
                        print("error \(error)")
    
                        let newEvent:EKEvent = EKEvent(eventStore: eventStore)
    
                        newEvent.title = currentInfo.title
                        newEvent.startDate = Calendar.current.date(byAdding: .hour, value: 8, to: currentInfo.start)!
                        newEvent.endDate = Calendar.current.date(byAdding: .hour, value: 12, to: newEvent.startDate)!
                        newEvent.notes = currentInfo.desc + "\nLink: " + currentInfo.url
                        newEvent.calendar = eventStore.defaultCalendarForNewEvents
                        newEvent.addAlarm(EKAlarm(absoluteDate: Calendar.current.date(byAdding: .hour, value: 7, to: currentInfo.start)!))
                        newEvent.location = currentInfo.location
    
                        do{
                            try eventStore.save(newEvent, span: EKSpan.thisEvent, commit: true)
                            DispatchQueue.main.async(execute: { () -> Void in
                                let alert = UIAlertController(title: "Event Added", message: "Thank you for adding this event!", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                            })
                            print("Saved Event")
                        }
                        catch{
                            print(error)
                        }
    
                }
            })

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let subCategory:calOppViewer = segue.destination as! calOppViewer
        
        if let indexPath = eventList.indexPathForSelectedRow {
            // do the work here
            
            let artObj = self.events[self.dateIndex + indexPath.row].url
            
            subCategory.urlString = artObj
        }
    }



}
extension calendar: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        /*let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        
        let startDate = formatter.date(from: "2016 02 01")! // You can use date generated from a formatter
        let endDate = Date()                                // You can also use dates created from this function
        
        */
        
        let startDate = Date()
        let endDate = Calendar.current.date(byAdding: .month, value: 6, to: startDate)
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate!,
                                                 numberOfRows: 6, // Only 1, 2, 3, & 6 are allowed
            calendar: Calendar.current,
            generateInDates: .forAllMonths,
            generateOutDates: .tillEndOfGrid,
            firstDayOfWeek: .sunday)
        
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplayCell cell: JTAppleDayCellView, date: Date, cellState: CellState) {
        let myCustomCell = cell as! cView
        
        // Setup Cell text
        myCustomCell.dayLabel.text = cellState.text
        
        
        // Setup text color
        if cellState.dateBelongsTo == .thisMonth {
            myCustomCell.dayLabel.textColor = UIColor.black
        } else {
            myCustomCell.dayLabel.textColor = UIColor.gray
        }
        var components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: Date())
        components.hour = 0
        components.minute = 0
        components.second = 0
        
        let date = Calendar.current.date(from: components)!
        
        if cellState.date == date{
            myCustomCell.todayView.layer.cornerRadius =  25
            myCustomCell.todayView.isHidden = false
        }
        else{
            myCustomCell.todayView.isHidden = true
        }
        
        if(startDates.contains(cellState.date)){
            let count = startDates.filter({ $0 == cellState.date}).count
            if(count == 1){
                myCustomCell.eventView.isHidden = false
            }
            else if(count == 2){
                myCustomCell.eventView.isHidden = false
                myCustomCell.eventViewTwo.isHidden = false
            }
            else{
                myCustomCell.eventView.isHidden = false
                myCustomCell.eventViewTwo.isHidden = false
                myCustomCell.eventPlus.isHidden = false
            }
            print(count)
        }
        else{
            myCustomCell.eventView.isHidden = true
            myCustomCell.eventViewTwo.isHidden = true
            myCustomCell.eventPlus.isHidden = true
        }
        
        handleCellSelection(view: cell, cellState: cellState)
    }
    
    // This sets the height of your header
    func calendar(_ calendar: JTAppleCalendarView, sectionHeaderSizeFor range: (start: Date, end: Date), belongingTo month: Int) -> CGSize {
        return CGSize(width: 200, height: 100)
    }
    
    
    // This setups the display of your header
    func calendar(_ calendar: JTAppleCalendarView, willDisplaySectionHeader header: JTAppleHeaderView, range: (start: Date, end: Date), identifier: String) {
        let startDate = range.start
        
        let month = Calendar.current.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        let year = Calendar.current.component(.year, from: startDate)
        let headerCell = header as? calHeader
        headerCell?.title.text = monthName
        headerCell?.yearTitle.text = String(year)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
    }
    
    func handleCellSelection(view: JTAppleDayCellView?, cellState: CellState) {
        guard let myCustomCell = view as? cView  else {
            return
        }
        
        if cellState.isSelected {
            myCustomCell.layer.borderColor = UIColor(red: 0, green: 0.6471, blue: 0.898, alpha: 1.0).cgColor /* #00a5e5 */
            myCustomCell.layer.borderWidth = 2
            self.dateEvtCount = self.startDates.filter({ $0 == cellState.date}).count
            if(self.dateEvtCount > 0){
                self.dateIndex = self.startDates.index(where: {$0 == cellState.date})!
                DispatchQueue.main.async(execute: { () -> Void in
                    self.eventList.reloadData()
                })
            }
        } else {
            myCustomCell.layer.borderWidth = 0
            self.dateEvtCount = 0
            self.dateIndex = 0
            DispatchQueue.main.async(execute: { () -> Void in
                self.eventList.reloadData()
            })
        }
    }
    
}
