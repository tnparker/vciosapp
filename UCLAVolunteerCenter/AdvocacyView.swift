//
//  AdvocacyView.swift
//  UCLAVolunteerCenter
//
//  Created by Taylor Parker on 8/19/16.
//  Copyright © 2016 UCLA. All rights reserved.
//

import UIKit
//import JTAppleCalendar

class AdvocacyView: UIViewController {
/*
    @IBOutlet var calendarView: JTAppleCalendarView!
    @IBOutlet weak var monthLabel: UILabel!
    
    
    let testCalendar: NSCalendar! = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
    let theMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
    let screenSize = UIScreen.mainScreen().bounds
 */
    
    @IBOutlet weak var webV: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        /*self.calendarView.dataSource = self
        self.calendarView.delegate = self
        self.calendarView.registerCellViewXib(fileName: "CellView")
        
        calendarView.cellInset = CGPoint(x: 0, y: 0)
        calendarView.firstDayOfWeek = DaysOfWeek.Sunday
        calendarView.scrollEnabled = false
        
        let i = calendarView.currentCalendarDateSegment().month
        monthLabel.text = theMonths[i]*/
        
        let url = URL (string: "http://advocacy.ucla.edu/");
        let requestObj = URLRequest(url: url!);
        webV.loadRequest(requestObj);
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
/*
    @IBAction func frontMonth(sender: AnyObject) {
        
        let i = calendarView.currentCalendarDateSegment().month
        monthLabel.text = theMonths[i]
        calendarView.scrollToNextSegment()
    }
    
    @IBAction func backMonth(sender: AnyObject) {
        
        let i = calendarView.currentCalendarDateSegment().month
        monthLabel.text = theMonths[i]
        calendarView.scrollToPreviousSegment()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    */
    
    

}
/*
extension AdvocacyView: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate  {
    // Setting up manditory protocol method
    func configureCalendar(calendar: JTAppleCalendarView) -> (startDate: NSDate, endDate: NSDate, numberOfRows: Int, calendar: NSCalendar) {
        // You can set your date using NSDate() or NSDateFormatter. Your choice.
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        
        let firstDate = formatter.dateFromString("2016 01 05")
        let secondDate = NSDate()
        let numberOfRows = 6
        let aCalendar = NSCalendar.currentCalendar() // Properly configure your calendar to your time zone here
        
        return (startDate: firstDate!, endDate: secondDate, numberOfRows: numberOfRows, calendar: aCalendar)
    }
    
    func calendar(calendar: JTAppleCalendarView, isAboutToDisplayCell cell: JTAppleDayCellView, date: NSDate, cellState: CellState) {
        (cell as! CellView).setupCellBeforeDisplay(cellState, date: date)
    }
    
    
}
 */


