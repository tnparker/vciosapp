# README #

The UCLA Volunteer Center App for iOS

### What is this repository for? ###

* Holds all the files to run, edit, etc. the iOS app known as "Volunteer"
* Safe Keeping
* Proof the app is actually fully functioning even though it cannot be downloaded from the App Store

### But I want the app, Master Programmer. ###

* Because of the nature of iOS, this app can only be used when a person is given the correct credentials/access since it isn't on the App Store. 
* To be granted access, contact tparker@volunteer.ucla.edu stating you are interested in the app and we will work with you to get you certified

### Who do I talk to? ###

* Any Questions? Contact tparker@volunteer.ucla.edu